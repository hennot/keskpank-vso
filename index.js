const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const routes = require('./routes');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./docs/api.yaml');
const {RequestBodyIsValidJson, RequestHeadersHaveCorrectContentType} = require('./middlewares');

// Read .env file
dotenv.config();

// Connect to mongodb
mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log('Connected to MongoDB');
});

// Init express
const app = express();

// Middlewares
app.use(RequestHeadersHaveCorrectContentType)
app.use(express.json())
app.use(RequestBodyIsValidJson)

// Routes
app.use('/', routes)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Run the app
app.listen(3000);