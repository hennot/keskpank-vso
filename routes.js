const express = require('express')
const router = express.Router()
const bankController = require('./controllers/bankController')
const { validateApiKey } = require('./middlewares')

router.post('/banks', bankController.register);
router.get('/banks', validateApiKey, bankController.getAll);

module.exports = router