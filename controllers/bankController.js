const bankModel = require('../models/bankModel')

function randomString(format) {
    var d = new Date().getTime();

    return format.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

async function register(req, res) {
    const {name, transactionUrl, owners, jwksUrl} = req.body
    try {

        // Insert new bank into db
        const bank = await bankModel.create({
            name,
            apiKey: randomString('xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'),
            transactionUrl,
            bankPrefix: randomString('xxx'),
            owners,
            jwksUrl
        })

        // Return result
        res.json({
            name: bank.name,
            apiKey: bank.apiKey,
            transactionUrl: bank.transactionUrl,
            bankPrefix: bank.bankPrefix,
            owners: bank.owners,
            jwksUrl: bank.jwksUrl
        })

    } catch (e) {
        res.statusCode = 400
        res.json({error: e.message})
    }

}
async function getAll(req, res) {

    // Get banks from DB
    const banks = await bankModel.find().select('transactionUrl -_id name owners jwksUrl apiKey bankPrefix').exec();

    // Return banks
    res.json(banks)
}

module.exports = {
    register, getAll
}