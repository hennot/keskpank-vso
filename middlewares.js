const bankModel = require('./models/bankModel')

exports.RequestBodyIsValidJson = (err, req, res, next) => {
    // body-parser will set this to 400 if the json is in error
    if (err.status === 400)
        return res.status(err.status).json({error: 'Malformed JSON'});
    return next(err); // if it's not a 400, let the default error handling do it.
}

exports.RequestHeadersHaveCorrectContentType = (req, res, next) => {
    // Catch invalid Content-Types
    var RE_CONTYPE = /^application\/(?:x-www-form-urlencoded|json)(?:[\s;]|$)/i;
    if (req.method !== 'GET' && !RE_CONTYPE.test(req.headers['content-type'])) {
        return res.status(406).json({error: 'Content-Type is not application/json'});
    }
    next();
}

exports.validateApiKey = async (req, res, next) => {

    // Check API key is provided
    const ApiKey = req.header('Api-Key');
    if (!ApiKey) return res.status(400).json({error: 'Missing Api-Key header'});

    // Validate API key
    const bank = await bankModel.findOne({apiKey: ApiKey});
    if (!bank) return res.status(400).json({error: 'Invalid Api-Key'});

    return next(); // Pass the request to the next middleware
}